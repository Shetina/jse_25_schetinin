package ru.t1.schetinin.tm.exception.entity;

public class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}